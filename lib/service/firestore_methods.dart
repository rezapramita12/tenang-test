import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:doctor_test/model/post_model.dart';
import 'package:doctor_test/service/storage_methods.dart';
import 'package:uuid/uuid.dart';

class FirestoreMethods {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  Future<String> uploadPost(
    String description,
    String uid,
    Uint8List file,
    String username,
  ) async {
    String result = "Some error occurred";
    try {
      String photoUrl = await StorageMethods().uploadImageToStorage(
        'posts',
        file,
        true,
      );
      String postId = const Uuid().v1();
      PostModel postModel = PostModel(
        description: description,
        datePublished: DateTime.now(),
        username: username,
        uid: uid,
        postId: postId,
        postUrl: photoUrl,
        likes: [],
      );
      _firestore.collection('posts').doc(postId).set(
            postModel.toJson(),
          );
      result = "success";
    } catch (err) {
      result = err.toString();
    }
    return result;
  }
}
