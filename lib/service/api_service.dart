import 'package:dio/dio.dart';

class ApiService {
  final Dio _dio = Dio();

  Future<Response?> getUser() async {
    final response =
        await _dio.get('https://jsonplaceholder.typicode.com/users');
    try {
      return response;
    } catch (e) {
      return response;
    }
  }
}
