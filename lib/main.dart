import 'package:doctor_test/splash_page.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'bloc/service/service_bloc.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => ServiceBloc(),
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Doctor Test',
        theme: ThemeData(
          primarySwatch: Colors.blue,
        ),
        // home: user == null ? LoginScreen() : const BottomNav(0),
        home: const SplashPage(),
      ),
    );
  }
}
