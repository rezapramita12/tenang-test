import 'dart:developer';

import 'package:doctor_test/color_helper.dart';
import 'package:doctor_test/ui/login_screen.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({Key? key}) : super(key: key);

  @override
  _OnBoardingScreenState createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => LoginScreen()),
    );
  }

  Widget _buildFullscreenImage(assets) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      decoration: BoxDecoration(
          image: DecorationImage(image: AssetImage(assets), fit: BoxFit.cover)),
    );
  }

  Widget titleDecoration({String? title, subtitle, Widget? widget}) {
    return Container(
      width: double.infinity,
      height: 400,
      decoration: const BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(50),
              bottomRight: Radius.circular(50))),
      alignment: Alignment.topCenter,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            title ?? '',
            style: const TextStyle(
              fontSize: 30.0,
              fontWeight: FontWeight.bold,
            ),
          ),
          const SizedBox(
            height: 30.0,
          ),
          Text(
            "$subtitle",
            style: const TextStyle(fontSize: 16, letterSpacing: 2),
            textAlign: TextAlign.center,
          ),
          const SizedBox(
            height: 57.0,
          ),
          widget ?? Container()
          // Text("Ipsum, giving information origins"),
          // Text("as well as a random"),
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);

    const pageDecoration = PageDecoration(
        titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
        bodyTextStyle: bodyStyle,
        titlePadding: EdgeInsets.zero,
        contentMargin: EdgeInsets.zero,
        // imageFlex: 0,
        bodyFlex: 210,
        bodyPadding: EdgeInsets.zero,
        // pageColor: Colors.white,
        // imagePadding: EdgeInsets.zero,
        fullScreen: true);

    return IntroductionScreen(
      key: introKey,
      globalBackgroundColor: Colors.white,

      pages: [
        PageViewModel(
          titleWidget: titleDecoration(
              title: 'Welcome to Alesha',
              subtitle:
                  'Reference site about Lorem \nIpsum, giving information origins\nas well as a random'),
          body: '',
          image: _buildFullscreenImage('assets/onboarding1.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          titleWidget: titleDecoration(
              title: 'Find Best Doctors',
              subtitle:
                  'Reference site about Lorem \nIpsum, giving information origins\nas well as a random'),
          body: "",
          image: _buildFullscreenImage('assets/onboarding2.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          titleWidget: titleDecoration(
            title: 'Get Fitness Trips',
            subtitle:
                'Reference site about Lorem \nIpsum, giving information origins\nas well as a random',
            widget: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: ColorHelper.primary,
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(25)),
                  minimumSize: const Size(315, 50)),
              onPressed: () async {
                log('sini');
                SharedPreferences pref = await SharedPreferences.getInstance();
                pref.setInt('boarding', 1);
                // ignore: use_build_context_synchronously
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => LoginScreen()),
                    (route) => false);
              },
              child: const Text("Continue"),
            ),
          ),
          body: "",
          image: _buildFullscreenImage('assets/onboarding3.png'),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: false,
      skipOrBackFlex: 0,
      nextFlex: 0,
      showBackButton: false,
      //rtl: true, // Display as right-to-left
      // back: const Icon(Icons.arrow_back),

      showNextButton: false,
      showDoneButton: false,
      skip: const Text('', style: TextStyle(fontWeight: FontWeight.w600)),
      // next: const Icon(Icons.arrow_forward),
      done: const Text('', style: TextStyle(fontWeight: FontWeight.w600)),
      // curve: Curves.easeIn,
      controlsMargin: const EdgeInsets.all(30),
      controlsPadding: const EdgeInsets.fromLTRB(200.0, 4.0, 8.0, 4.0),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        activeColor: Color(0xff3462FF),
        color: Colors.white,
        activeSize: Size(10.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
      dotsContainerDecorator: const ShapeDecoration(
        // color: Colors.black87,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }
}
