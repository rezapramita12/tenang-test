part of 'service_bloc.dart';

abstract class ServiceEvent extends Equatable {}

class GetListUser extends ServiceEvent {
  @override
  List<Object?> get props => [];
}

class SearchUser extends ServiceEvent {
  final String value;
  SearchUser(this.value);
  @override
  List<Object?> get props => [];
}
