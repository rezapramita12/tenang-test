part of 'service_bloc.dart';

abstract class ServiceState extends Equatable {
  const ServiceState();

  @override
  List<Object> get props => [];
}

class ServiceInitial extends ServiceState {}

class GetUserLoading extends ServiceState {}

class GetUserError extends ServiceState {
  final String message;
  const GetUserError(this.message);
}

class GetUserSuccess extends ServiceState {
  final List<ResponseUser> user;
  const GetUserSuccess(this.user);
}

class SearchUserLoading extends ServiceState {}

class SearchUserError extends ServiceState {
  final String message;
  const SearchUserError(this.message);
}

class SearchUserSuccess extends ServiceState {
  final List<ResponseUser> user;
  const SearchUserSuccess(this.user);
}
