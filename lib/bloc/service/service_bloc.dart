import 'dart:developer';

import 'package:bloc/bloc.dart';
import 'package:doctor_test/model/response_user.dart';
import 'package:doctor_test/service/api_service.dart';
import 'package:equatable/equatable.dart';

part 'service_event.dart';
part 'service_state.dart';

class ServiceBloc extends Bloc<ServiceEvent, ServiceState> {
  final ApiService _apiService = ApiService();
  List<ResponseUser> _responseUser = [];
  List<ResponseUser> get responseUser => _responseUser;
  List<ResponseUser> _responseUserSearch = [];
  List<ResponseUser> get responseUserSearch => _responseUserSearch;

  ServiceBloc() : super(ServiceInitial()) {
    on<GetListUser>((event, emit) async {
      emit(GetUserLoading());
      try {
        final response = await _apiService.getUser();
        if (response!.statusCode == 200) {
          log('masuk success data');
          List responseList = response.data;
          List<ResponseUser> listData =
              responseList.map((f) => ResponseUser.fromJson(f)).toList();
          _responseUser = listData;
          emit(GetUserSuccess(_responseUser));
        } else {
          log('masuk bad data ');
          emit(GetUserError(response.data['message']));
        }
      } catch (e) {
        log('masuk error data ${e.toString()}');
        emit(GetUserError(e.toString()));
      }
      // TODO: implement event handler
    });
    on<SearchUser>((event, emit) async {
      emit(SearchUserLoading());
      try {
        final response = await _apiService.getUser();
        if (response!.statusCode == 200) {
          log('masuk success data');
          List responseList = response.data;
          List<ResponseUser> listData =
              responseList.map((f) => ResponseUser.fromJson(f)).toList();
          _responseUserSearch = listData
              .where(
                (element) => element.name!.toLowerCase().contains(event.value),
              )
              .toList();
          log(_responseUserSearch.length.toString());
          emit(SearchUserSuccess(_responseUserSearch));
        } else {
          log('masuk bad data ');
          emit(SearchUserError(response.data['message']));
        }
      } catch (e) {
        emit(SearchUserError(e.toString()));
      }
    });
  }
}
