// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'dart:developer';

import 'package:doctor_test/color_helper.dart';
import 'package:doctor_test/service/firebase_service.dart';
import 'package:doctor_test/ui/login_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key? key}) : super(key: key);

  @override
  State<ProfilePage> createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  User? user = FirebaseAuth.instance.currentUser;
  @override
  Widget build(BuildContext context) {
    log(user.toString());
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          margin: const EdgeInsets.only(top: 200),
          child: Center(
            child: Column(
              children: [
                user?.photoURL == null
                    ? Container(
                        padding: const EdgeInsets.all(16),
                        decoration: BoxDecoration(
                            border: Border.all(
                                width: 1, color: ColorHelper.primary)),
                        child: const Icon(
                          Icons.person,
                          size: 40,
                        ),
                      )
                    : Image.network(
                        "${user?.photoURL}",
                        width: 64.0,
                        height: 64.0,
                        fit: BoxFit.fill,
                      ),
                const SizedBox(
                  height: 20.0,
                ),
                Text(
                  user?.displayName ?? '-',
                  style: const TextStyle(fontSize: 13.0, color: Colors.black),
                ),
                const SizedBox(
                  height: 10.0,
                ),
                Text(
                  "${user?.email}",
                  style: const TextStyle(
                    fontSize: 13.0,
                  ),
                ),
                const SizedBox(
                  height: 30.0,
                ),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.red,
                  ),
                  onPressed: () async {
                    FirebaseService service = FirebaseService();
                    try {
                      await service.signOutFromGoogle();
                      // ignore: use_build_context_synchronously
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(builder: (context) => LoginScreen()),
                        (route) => false,
                      );
                      log('yesy baerhasil');
                    } catch (e) {
                      if (e is FirebaseAuthException) {
                        log(e.message!);
                      }
                    }
                  },
                  child: const Text("Logout"),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
