import 'package:doctor_test/service/auth_methods.dart';
import 'package:doctor_test/ui/login_screen.dart';
import 'package:doctor_test/utils/utils.dart';
import 'package:flutter/material.dart';

class SignupScreen extends StatefulWidget {
  @override
  _SignupScreenState createState() => _SignupScreenState();
}

class _SignupScreenState extends State<SignupScreen> {
  final _textStyleGrey = const TextStyle(fontSize: 12.0, color: Colors.grey);
  final _textStylePurple =
      const TextStyle(fontSize: 12.0, color: Color(0xff4A80FF));
  final _formKey = GlobalKey<FormState>();
  String? _name;
  String? _email;
  String? _password;
  bool rememberMe = false;
  bool _isLoading = false;

  void _signUp() async {
    if (_formKey.currentState!.validate()) {
      if (rememberMe) {
        _formKey.currentState!.save();
        setState(() {
          _isLoading = true;
        });
        // Logging in the user w/ Firebase
        String result = await AuthMethods().signUpUser(
            name: _name, email: _email, password: _password, username: _name);
        if (result != 'success') {
          // ignore: use_build_context_synchronously
          showSnackBar(result, context);
        } else {
          // ignore: use_build_context_synchronously
          Navigator.pop(context);
        }
        setState(() {
          _isLoading = false;
        });
      } else {
        showSnackBar('Please check if you Agree with term', context);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottomBar(),
      appBar: PreferredSize(
          //wrap with PreferredSize
          preferredSize: const Size.fromHeight(50), //height of appbar
          child: AppBar(
            elevation: 0,
            title: Container(
              margin: const EdgeInsets.all(10),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: const [
                      Text("Sign Up",
                          style: TextStyle(color: Colors.black, fontSize: 16)),
                      Text("create new account",
                          style: TextStyle(color: Colors.grey, fontSize: 16)),
                    ],
                  ),
                ],
              ),
            ), //appbar title
            backgroundColor: Colors.white10,
            leading: InkWell(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.arrow_back_ios,
                color: Colors.black,
              ),
            ), //appbar background color
          )),
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          margin: const EdgeInsets.only(left: 10, right: 10),
          child: Column(
            children: <Widget>[
              const SizedBox(
                height: 16.0,
              ),
              Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 30.0,
                        vertical: 10.0,
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                            floatingLabelBehavior: FloatingLabelBehavior.never,
                            fillColor: Colors.grey.withOpacity(0.1),
                            filled: true,
                            border: OutlineInputBorder(
                                borderSide: const BorderSide(
                                  width: 0,
                                  style: BorderStyle.none,
                                ),
                                borderRadius: BorderRadius.circular(20)),
                            isDense: true,
                            labelText: 'Full Name'),
                        validator: (input) => input!.trim().isEmpty
                            ? 'Please enter a valid name'
                            : null,
                        onSaved: (input) => _name = input!,
                      ),
                    ),
                    const Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: 30.0,
                        // vertical: 10.0,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 30.0,
                        // vertical: 10.0,
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Email',
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          fillColor: Colors.grey.withOpacity(0.1),
                          filled: true,
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                width: 0,
                                style: BorderStyle.none,
                              ),
                              borderRadius: BorderRadius.circular(20)),
                          isDense: true,
                        ),
                        validator: (input) => !input!.contains('@')
                            ? 'Please enter a valid email'
                            : null,
                        onSaved: (input) => _email = input!,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 30.0,
                        vertical: 10.0,
                      ),
                      child: TextFormField(
                        decoration: InputDecoration(
                          labelText: 'Password',
                          floatingLabelBehavior: FloatingLabelBehavior.never,
                          fillColor: Colors.grey.withOpacity(0.1),
                          filled: true,
                          border: OutlineInputBorder(
                              borderSide: const BorderSide(
                                width: 0,
                                style: BorderStyle.none,
                              ),
                              borderRadius: BorderRadius.circular(20)),
                          isDense: true,
                        ),
                        validator: (input) => input!.length < 6
                            ? 'Must be at least 6 characters'
                            : null,
                        onSaved: (input) => _password = input!,
                        obscureText: true,
                      ),
                    ),
                    Row(mainAxisAlignment: MainAxisAlignment.start, children: [
                      Container(
                        margin: const EdgeInsets.only(left: 16),
                        child: Checkbox(
                            focusColor: const Color(0xff4A80FF),
                            activeColor: const Color(0xff4A80FF),
                            value: rememberMe,
                            onChanged: (newValue) {
                              setState(() => rememberMe = newValue!);
                            }),
                      ),
                      //  SizedBox(width: 10.0),
                      Container(
                          margin: const EdgeInsets.only(top: 20),
                          child: const Text(
                              'By creating an account you agree to\nour Terms of Service and Privacy Policy')),
                    ]),
                    const SizedBox(
                      height: 30.0,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        left: 30,
                        right: 30,
                      ),
                      child: Container(
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          color: const Color(0xff4A80FF),
                        ),
                        child: TextButton(
                          onPressed: () => _signUp(),
                          child: const Text(
                            'Sign Up',
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 18.0,
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget _bottomBar() {
    return Container(
      alignment: Alignment.center,
      height: 49.5,
      child: Column(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // Container(
              //   height: 1.0,
              //   color: Colors.grey.withOpacity(0.7),
              // ),
              Padding(
                  padding: const EdgeInsets.only(bottom: 0.5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Already have an account?', style: _textStyleGrey),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => LoginScreen()));
                        },
                        // Navigator.pushNamed(context, SignupScreen.id),
                        child: Text('Sign In!', style: _textStylePurple),
                      ),
                    ],
                  )),
            ],
          ),
        ],
      ),
    );
  }
}
