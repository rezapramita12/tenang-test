import 'dart:developer';

import 'package:doctor_test/service/firebase_service.dart';
import 'package:doctor_test/service/auth_methods.dart';
import 'package:doctor_test/ui/bottom_nav.dart';
import 'package:doctor_test/ui/signup_screen.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'dart:io';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final TextEditingController _emailIdController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  bool _isLoading = false;

  final _textStyleBlack = const TextStyle(fontSize: 12.0, color: Colors.black);
  final _textStyleGrey = const TextStyle(fontSize: 12.0, color: Colors.grey);
  final _textStylePrimary = const TextStyle(
      fontSize: 12.0, color: Color(0xff4A80FF), fontWeight: FontWeight.bold);

  @override
  void dispose() {
    super.dispose();
    _emailIdController.dispose();
    _passwordController.dispose();
  }

  void _logInUser() async {
    if (_emailIdController.text.isEmpty) {
      _showEmptyDialog("Type something");
    } else if (_passwordController.text.isEmpty) {
      _showEmptyDialog("Type something");
    }
    setState(() {
      _isLoading = true;
    });
    String result = await AuthMethods().logInUser(
      email: _emailIdController.text,
      password: _passwordController.text,
    );
    if (result == 'successs') {
    } else {
      // ignore: use_build_context_synchronously
      Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => const BottomNav(
                    autoSelectMenu: 0,
                  )));
      // showSnackBar(result, context);
    }
    setState(() {
      _isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottomBar(),
      body: _body(),
    );
  }

  Widget _userIDEditContainer() {
    return TextField(
      controller: _emailIdController,
      decoration: InputDecoration(
          fillColor: Colors.grey.withOpacity(0.2),
          filled: true,
          prefixIcon: Image.asset(
            'assets/email.png',
            width: 10,
          ),
          hintText: 'Phone number, email or username',
          border: OutlineInputBorder(
              // borderSide: const BorderSide(color: Colors.black),
              borderSide: const BorderSide(
                width: 0,
                style: BorderStyle.none,
              ),
              borderRadius: BorderRadius.circular(20)),
          isDense: true),
      style: _textStyleBlack,
    );
  }

  Widget _passwordEditContainer() {
    return Container(
      padding: const EdgeInsets.only(top: 5.0),
      child: TextField(
        controller: _passwordController,
        obscureText: true,
        keyboardType: TextInputType.visiblePassword,
        decoration: InputDecoration(
            fillColor: Colors.grey.withOpacity(0.2),
            prefixIcon: Image.asset(
              'assets/lock.png',
              width: 10,
            ),
            filled: true,
            hintText: 'Password',
            disabledBorder: InputBorder.none,
            border: OutlineInputBorder(
                // borderSide: BorderSide(color: Colors.black),
                borderSide: const BorderSide(
                  width: 0,
                  style: BorderStyle.none,
                ),
                borderRadius: BorderRadius.circular(20)),
            isDense: true),
        style: _textStyleBlack,
      ),
    );
  }

  Widget _loginContainer() {
    return GestureDetector(
      onTap: _logInUser,
      child: Container(
        alignment: Alignment.center,
        margin: const EdgeInsets.only(top: 10.0),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20),
            color: const Color(0xff4A80FF)),
        width: 500.0,
        height: 50.0,
        child: _isLoading
            ? const Center(
                child: CircularProgressIndicator(
                  color: Colors.white,
                ),
              )
            : const Text(
                "Sign In",
                style: TextStyle(color: Colors.white),
              ),
      ),
    );
  }

  Widget _bottomBar() {
    return Container(
      alignment: Alignment.center,
      height: 49.5,
      child: Column(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              // Container(
              //   height: 1.0,
              //   color: Colors.grey.withOpacity(0.7),
              // ),
              Padding(
                  padding: const EdgeInsets.only(bottom: 0.5),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text('Do not have an account?', style: _textStyleGrey),
                      TextButton(
                        onPressed: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignupScreen()));
                        },
                        // Navigator.pushNamed(context, SignupScreen.id),
                        child: Text('Sign Up!', style: _textStylePrimary),
                      ),
                    ],
                  )),
            ],
          ),
        ],
      ),
    );
  }

  Widget _body() {
    return SingleChildScrollView(
      child: Container(
        // alignment: Alignment.center,
        padding: const EdgeInsets.all(40.0),
        child: Column(
          // mainAxisAlignment: MainAxisAlignment.start,
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.only(top: 50),
                child: Container(
                  width: 150,
                  height: 100,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/doctor.png'))),
                )),
            Padding(
                padding: const EdgeInsets.only(bottom: 5.0),
                child: Container(
                  width: 150,
                  height: 100,
                  decoration: const BoxDecoration(
                      image: DecorationImage(
                          image: AssetImage('assets/PLOT.png'))),
                )),
            const SizedBox(
              height: 20,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                Text(
                  'Welcome Back!',
                  style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
                ),
                // Text('data'),
              ],
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.start,
              children: const [
                Text('sign in to continue.',
                    style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.w400,
                        color: Colors.grey)),
                // Text('data'),
              ],
            ),
            const SizedBox(
              height: 20,
            ),
            _userIDEditContainer(),
            const SizedBox(
              height: 10,
            ),
            _passwordEditContainer(),
            Container(
              margin: const EdgeInsets.only(top: 10, bottom: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.end,
                children: const [
                  Text('Forgot Password ?',
                      style: TextStyle(
                          fontWeight: FontWeight.w400,
                          color: Color(0xff4A80FF))),
                  // Text('data'),
                ],
              ),
            ),
            _loginContainer(),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const [
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Text('/'),
                )
              ],
            ),

            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                InkWell(
                  onTap: () async {
                    FirebaseService service = FirebaseService();
                    try {
                      await service.signInwithGoogle();
                      // ignore: use_build_context_synchronously
                      Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const BottomNav(
                                  autoSelectMenu: 0,
                                )),
                        (route) => false,
                      );
                      log('yesy baerhasil');
                    } catch (e) {
                      if (e is FirebaseAuthException) {
                        log(e.message!);
                      }
                    }
                  },
                  child: Container(
                    margin: const EdgeInsets.all(20),
                    // padding: EdgeInsets.all(5),
                    width: 50,
                    height: 50,
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.grey, width: 2),
                        borderRadius: BorderRadius.circular(20) //
                        ),
                    child: Center(
                        child: SvgPicture.asset(
                      width: 20,
                      'assets/icons/Icon_Google.svg',
                    )),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  // padding: EdgeInsets.all(5),
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 2),
                      borderRadius: BorderRadius.circular(20) //
                      ),
                  child: Center(
                      child: SvgPicture.asset(
                    width: 20,
                    'assets/icons/Icon_Apple.svg',
                  )),
                ),
                Container(
                  margin: const EdgeInsets.all(20),
                  // padding: EdgeInsets.all(5),
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                      border: Border.all(color: Colors.grey, width: 2),
                      borderRadius: BorderRadius.circular(20) //
                      ),
                  child: Center(
                      child: SvgPicture.asset(
                          width: 15, 'assets/icons/Icon_facebook.svg')),
                ),
              ],
            ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: <Widget>[
            //     Text(
            //       'Forgot your login details?',
            //       style: _textStyleGrey,
            //     ),
            //     TextButton(
            //       onPressed: () {},
            //       child: Text(
            //         'Get help logging in.',
            //         style: _textStyleBlueGrey,
            //       ),
            //     )
            //   ],
            // ),
            // Row(
            //   mainAxisAlignment: MainAxisAlignment.center,
            //   children: <Widget>[
            //     Container(
            //       height: 1.0,
            //       width: MediaQuery.of(context).size.width / 2.7,
            //       color: Colors.grey,
            //       child: ListTile(),
            //     ),
            //     Text(
            //       ' OR ',
            //       style: TextStyle(color: Colors.blueGrey),
            //     ),
            //     Container(
            //       height: 1.0,
            //       width: MediaQuery.of(context).size.width / 2.7,
            //       color: Colors.grey,
            //     ),
            //   ],
            // ),
            // // _facebookContainer()
          ],
        ),
      ),
    );
  }

  _showEmptyDialog(String title) {
    if (Platform.isAndroid) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => AlertDialog(
          content: Text("$title can't be empty"),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("OK"))
          ],
        ),
      );
    } else if (Platform.isIOS) {
      showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) => CupertinoAlertDialog(
          content: Text("$title can't be empty"),
          actions: <Widget>[
            TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                },
                child: const Text("OK"))
          ],
        ),
      );
    }
  }
}
