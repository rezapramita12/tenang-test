// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'dart:developer';

import 'package:doctor_test/color_helper.dart';
import 'package:doctor_test/model/response_user.dart';
import 'package:flutter/material.dart';

import '../bloc/service/service_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final ServiceBloc _serviceBloc = ServiceBloc();

  final TextEditingController _search = TextEditingController();
  List<ResponseUser>? search;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.read<ServiceBloc>().add(GetListUser());
  }

  void searchByText(value) {
    log(value.toString());
    if (value == null || value == '') {
      log('sini');
      setState(() {
        search = context.read<ServiceBloc>().responseUser;
      });
    } else {
      List<ResponseUser> tempSearch = [];
      tempSearch = search!
          .where((element) => element.name!.toLowerCase().contains(value))
          .toList();
      setState(() {
        search = tempSearch;
      });
      log(search!.length.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              BlocConsumer<ServiceBloc, ServiceState>(
                listener: (context, state) {
                  if (state is GetUserSuccess) {}
                  // TODO: implement listener
                },
                builder: (context, state) {
                  if (state is GetUserLoading) {
                    return Center(
                      child: CircularProgressIndicator(
                        color: ColorHelper.primary,
                      ),
                    );
                  } else if (state is GetUserSuccess) {
                    return SizedBox(
                      height: 140.0,
                      child: ListView.builder(
                        itemCount: 3,
                        scrollDirection: Axis.horizontal,
                        itemBuilder: (context, index) {
                          ResponseUser data =
                              context.read<ServiceBloc>().responseUser[index];
                          return Container(
                            width: 140,
                            margin: const EdgeInsets.only(right: 10.0),
                            decoration: BoxDecoration(
                              image: const DecorationImage(
                                image: NetworkImage(
                                  "https://i.ibb.co/QrTHd59/woman.jpg",
                                ),
                                fit: BoxFit.cover,
                              ),
                              borderRadius: const BorderRadius.all(
                                Radius.circular(16.0),
                              ),
                              color: ColorHelper.primary,
                            ),
                            child: Stack(
                              children: [
                                Container(
                                  padding: const EdgeInsets.all(6.0),
                                  margin: const EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    color: ColorHelper.primary,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(
                                        12.0,
                                      ),
                                    ),
                                  ),
                                  child: const Text(
                                    "Best",
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 8.0,
                                    ),
                                  ),
                                ),
                                Positioned(
                                  bottom: 0,
                                  left: 0,
                                  right: 0,
                                  child: Container(
                                    padding: const EdgeInsets.all(12.0),
                                    decoration: const BoxDecoration(
                                      color: Colors.black38,
                                      borderRadius: BorderRadius.only(
                                        bottomLeft: Radius.circular(16.0),
                                        bottomRight: Radius.circular(16.0),
                                      ),
                                    ),
                                    child: Text(
                                      "${data.name}",
                                      overflow: TextOverflow.ellipsis,
                                      style: const TextStyle(
                                        fontSize: 11.0,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    );
                  } else if (state is GetUserError) {
                    return Text(state.message);
                  } else {
                    return Center(
                      child: CircularProgressIndicator(
                        color: ColorHelper.primary,
                      ),
                    );
                  }
                },
              ),
              const SizedBox(
                height: 16.0,
              ),
              Container(
                padding: const EdgeInsets.symmetric(
                  vertical: 2.0,
                  horizontal: 16.0,
                ),
                decoration: BoxDecoration(
                  color: Colors.grey[200],
                  borderRadius: const BorderRadius.all(
                    Radius.circular(12.0),
                  ),
                  border: Border.all(
                    width: 1.0,
                    color: Colors.grey[400]!,
                  ),
                ),
                child: Row(
                  children: [
                    const Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Icon(Icons.search),
                    ),
                    Expanded(
                      child: TextFormField(
                        initialValue: null,
                        controller: _search,
                        decoration: const InputDecoration.collapsed(
                          filled: true,
                          fillColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          hintText: "Search doctor",
                        ),
                        onFieldSubmitted: (value) {
                          log(value);
                          searchByText(value);
                        },
                        onChanged: (value) {
                          log(value);
                          searchByText(value);
                        },
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(
                height: 16.0,
              ),
              search == null
                  ? BlocConsumer<ServiceBloc, ServiceState>(
                      listener: (context, state) {
                        if (state is GetUserSuccess) {}
                        // TODO: implement listener
                      },
                      builder: (context, state) {
                        if (state is GetUserLoading) {
                          return Center(
                            child: CircularProgressIndicator(
                              color: ColorHelper.primary,
                            ),
                          );
                        } else if (state is GetUserSuccess) {
                          return ListView.builder(
                            itemCount:
                                context.read<ServiceBloc>().responseUser.length,
                            shrinkWrap: true,
                            physics: const ScrollPhysics(),
                            itemBuilder: (BuildContext context, int index) {
                              search = context.read<ServiceBloc>().responseUser;
                              ResponseUser data = context
                                  .read<ServiceBloc>()
                                  .responseUser[index];
                              return Card(
                                child: ListTile(
                                  title: Text(data.name ?? '-'),
                                  subtitle: Text(data.address?.city ?? '-'),
                                ),
                              );
                            },
                          );
                        } else if (state is GetUserError) {
                          return Text(state.message);
                        } else {
                          return Center(
                            child: CircularProgressIndicator(
                              color: ColorHelper.primary,
                            ),
                          );
                        }
                      },
                    )
                  : search!.isNotEmpty
                      ? ListView.builder(
                          itemCount: search!.length,
                          shrinkWrap: true,
                          physics: const ScrollPhysics(),
                          itemBuilder: (BuildContext context, int index) {
                            return Card(
                              child: ListTile(
                                title: Text("${search![index].name}"),
                                subtitle:
                                    Text("${search![index].address!.city}"),
                              ),
                            );
                          },
                        )
                      : const Text("Data Not Found")
            ],
          ),
        ),
      ),
    );
  }
}
