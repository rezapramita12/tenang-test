// ignore_for_file: camel_case_types, prefer_typing_uninitialized_variables

import 'package:doctor_test/color_helper.dart';
import 'package:doctor_test/model/response_user.dart';
import 'package:flutter/material.dart';

import '../bloc/service/service_bloc.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AppointmentPage extends StatefulWidget {
  const AppointmentPage({Key? key}) : super(key: key);

  @override
  State<AppointmentPage> createState() => _AppointmentPageState();
}

class _AppointmentPageState extends State<AppointmentPage> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    context.read<ServiceBloc>().add(GetListUser());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              BlocConsumer<ServiceBloc, ServiceState>(
                listener: (context, state) {
                  if (state is GetUserSuccess) {}
                  // TODO: implement listener
                },
                builder: (context, state) {
                  if (state is GetUserLoading) {
                    return Container(
                      margin: const EdgeInsets.only(top: 300),
                      alignment: Alignment.center,
                      child: CircularProgressIndicator(
                        color: ColorHelper.primary,
                      ),
                    );
                  } else if (state is GetUserSuccess) {
                    return ListView.builder(
                      itemCount:
                          context.read<ServiceBloc>().responseUser.length,
                      shrinkWrap: true,
                      physics: const ScrollPhysics(),
                      itemBuilder: (BuildContext context, int index) {
                        ResponseUser data =
                            context.read<ServiceBloc>().responseUser[index];
                        return Padding(
                          padding: const EdgeInsets.only(bottom: 16.0),
                          child: Card(
                            elevation: 2,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(16)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        "${DateTime.now().day} - ${DateTime.now().month} - ${DateTime.now().year}",
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold),
                                      ),
                                      Container(
                                        padding: const EdgeInsets.all(4.0),
                                        decoration: BoxDecoration(
                                          color: ColorHelper.primary,
                                          borderRadius:
                                              BorderRadius.circular(5),
                                        ),
                                        child: const Text(
                                          'Already Paid',
                                          style: TextStyle(
                                              fontSize: 12,
                                              color: Colors.white),
                                        ),
                                      )
                                    ],
                                  ),
                                  const SizedBox(
                                    height: 16.0,
                                  ),
                                  Text(
                                    "Dr. ${data.name}",
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold),
                                  ),
                                  const SizedBox(
                                    height: 2.0,
                                  ),
                                  const Text(
                                    "Heart Doctor",
                                    style: TextStyle(fontSize: 12),
                                  ),
                                  const SizedBox(
                                    height: 8.0,
                                  ),
                                  RichText(
                                    text: TextSpan(
                                      text: 'Consulting Fee ',
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        const TextSpan(
                                            text: ':',
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold)),
                                        TextSpan(
                                            text: ' Rp.50.000',
                                            style: TextStyle(
                                                color: ColorHelper.primary,
                                                fontWeight: FontWeight.bold)),
                                      ],
                                    ),
                                  ),
                                  const Divider(
                                    color: Colors.grey,
                                  ),
                                  Container(
                                    width: double.infinity,
                                    height: 40,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                        color: ColorHelper.primary,
                                        borderRadius:
                                            BorderRadius.circular(12)),
                                    child: const Text(
                                      "Pilih",
                                      style: TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.white,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        );
                      },
                    );
                  } else if (state is GetUserError) {
                    return Text(state.message);
                  } else {
                    return const Center(
                      child: CircularProgressIndicator(),
                    );
                  }
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
