import 'package:doctor_test/color_helper.dart';
import 'package:doctor_test/ui/appointment_page.dart';
import 'package:doctor_test/ui/doctor_page.dart';
import 'package:doctor_test/ui/home_page.dart';
import 'package:doctor_test/ui/profile_page.dart';
import 'package:flutter/material.dart';

//test
class BottomNav extends StatefulWidget {
  final int autoSelectMenu;

  const BottomNav({super.key, required this.autoSelectMenu});

  @override
  _BottomNavState createState() => _BottomNavState();
}

class _BottomNavState extends State<BottomNav> {
  var _selectedMenu = 0;
  final title = <String>["Home", "Inbox", "Appointment", "Profil"];

  @override
  void initState() {
    super.initState();

    setState(() {
      _selectedMenu = widget.autoSelectMenu;
    });
  }

  @override
  void didChangeDependencies() async {
    super.didChangeDependencies();

    _selectedMenu = widget.autoSelectMenu;
  }

  final List<Widget> _items = [
    const HomePage(),
    const DoctorPage(),
    const AppointmentPage(),
    const ProfilePage(),
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        elevation: 0,
        backgroundColor: Colors.white,
        title: Center(
          child: Text(title[_selectedMenu],
              style: const TextStyle(color: Colors.black)),
        ),
      ),
      body: _items[_selectedMenu],
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        currentIndex: _selectedMenu,
        onTap: (index) {
          setState(() {
            _selectedMenu = index;
          });
        },
        selectedItemColor: ColorHelper.primary,
        unselectedItemColor: Colors.grey,
        showSelectedLabels: true,
        showUnselectedLabels: true,
        selectedLabelStyle: TextStyle(color: ColorHelper.primary),
        unselectedLabelStyle: const TextStyle(color: Colors.grey),
        items: [
          BottomNavigationBarItem(
            icon: Icon(
              Icons.home,
              color: _selectedMenu == 0 ? ColorHelper.primary : Colors.grey,
            ),
            label: "Home",
          ),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.message_sharp,
                color: _selectedMenu == 1 ? ColorHelper.primary : Colors.grey,
              ),
              label: "Inbox"),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.schedule,
                color: _selectedMenu == 2 ? ColorHelper.primary : Colors.grey,
              ),
              label: "Appointment"),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.person,
                color: _selectedMenu == 3 ? ColorHelper.primary : Colors.grey,
              ),
              label: "Profile")
        ],
      ),
    );
  }
}
